@extends('layouts.app')

@section('content')
<h1>ubykuo - Prueba técnica Backend - Alejandro Nelis</h1>
<!-- Another variation with a button -->
    <form class="navbar-form" role="search">
        <div class="input-group">
            <input type="text" class="form-control" name="q" placeholder="GitHub Repository">
            <div class="input-group-append">
                <button class="btn btn-secondary" type="button">
                    <i class="fa fa-search"></i>
                </button>
            </div>
        </div>
    </form>
    <div class="card">
      <div class="card-header"><h2> GitHab</h2></div>
      <div class="card-body">
          <table class="table table-striped">
           <tr>
             <th>Name</th>
             <th>Description</th>
           </tr>
           @if(count($repos['items'])>0)
                @foreach($repos['items'] as $repo)
                   <tr>
                        <td><strong>{{ $repo['name'] }}</strong></td>
                        <td>{{ $repo['description'] }}</td>
                   </tr>
                @endforeach
            @endif
          </table>
      </div>
      <div class="card-footer">Total found {{ number_format($repos['total_count'],0) }}</div>
    </div>

    <div class="card">
      <div class="card-header"><h2> Directories with ".git"</h2></div>
      <div class="card-body">
        <table>
             @foreach($filesGit as $file)
             <tr>
               <td>{{ $file }}</td>
             </tr>
            @endforeach
        </table>
    </div>
    <div class="card-footer">Total {{ number_format(count($filesGit),0) }}</div>
</div>
@endsection
