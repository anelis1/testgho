<?php
/**
 * Created by Alejandro Nelis.
 * User: Alejandro Nelis
 * Date: 7/12/2018
 * Time: 13:19
 */

namespace App\Repository\Transformers;

class GithubTransformer extends Transformer{

    public function transform($github)
    {
        dd($github);
        return [
            'name' => $github['name'],
            'description' => $github['description'],
         ];
    }

}
