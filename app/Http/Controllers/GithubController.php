<?php

namespace app\http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiController;

class GithubController extends ApiController
{
    var $path = '/web/ministerio';

    public function index(Request $request)
    {
        try {
            $search = $request->get('q');
            if (!isset($search)) {
                $search = 'laravel';
            }
            $client = new \Github\Client();
            $repos = $client->api('search')->repositories($search);
            $filesGit = [];
            if (is_dir($this->path)) {
                $filesGit = $this->scanAllDir($this->path);
            }
            return View('github.repos', compact('repos', 'filesGit'));
        } catch (\RuntimeException $e) {
            dd($e);
            $this->handleAPIException($e);
        }
    }//index

    /**
     * @description: return from GitHub searching
     * @author:      Alejandro Nelis
     * @param:       Search String
     * @return:      Json String response
     */
    public function search(Request $request)
    {
        $search = $request->get('q');
        if (!isset($search)) {
            $search = 'laravel';
        }
        $client = new \Github\Client();
        $repos = $client->api('search')->repositories($search);
        if (count($repos) == 0) {
            return $this->respondWithError("No data found!");
        }
        $filesGit = [];
        if (is_dir($this->path)) {
            $filesGit = $this->scanAllDir($this->path);
        }
        return response()->json([
            'status' => true, 'message' => 'Records found!', 'data' => $repos, 'dataDirectories' => $filesGit
        ]);
    }

    /**
     * @description: returns a list of directories that found a sub directory called ".git"
     * @author:      Alejandro Nelis
     * @param:       Path search String
     * @return:      Array of directories
     */
    function scanAllDir($dir)
    {
        $result = [];
        $dirs = array_diff(scandir($dir), array('.', '..'));
        foreach ($dirs as $filename) {
            $filePath = $dir . '/' . $filename;
            if (is_dir($filePath)) {
                if ($filename = ".git") {
                    $result[] = $filePath;
                }
                foreach ($this->scanAllDir($filePath) as $childFilename) {
                    //recursion to find if there is more .git
                }
            }
        }
        return $result;
    }//scanDir
}
