<?php

public static function crearAdjudicacionDesdeOferta($oferta) {

    $productosOfertas = DB::table('ofertas_productos')
        ->where('oferta_id', $oferta->id)
        ->join('productos', 'productos.id', '=', 'ofertas_productos.producto_id')
        ->select('*', 'ofertas_productos.id as oferta_producto_id')
        ->get();
    $fechaInicio = $oferta->itemLicitacion->fecha_inicio;
    $fechaFin = $oferta->itemLicitacion->fecha_fin;
    foreach ($productosOfertas as $productoOf) {
        $adj = new Adjudicacion();
        $adj->oferta_producto_id = $productoOf->oferta_producto_id;
        $adj->empresa_id = $productoOf->empresa_id;
        $adj->fecha_de_inicio = $fechaInicio;
        $adj->fecha_de_fin = $fechaFin;
        $adj->estado_id = Self::ADJUDICADA;
        $adj->usuario_creador_id = $oferta->usuario_creador_id;
        $adj->empresa_solicitante_id = $oferta->itemLicitacion->licitacion->empresa_id;

        //Validar si existe la adjudicacion
        if ($adj::where('oferta_producto_id', $adj->oferta_producto_id)->first()) {
            return false;
        } else {
            //Si no existe la adjudicación hacer el save
            if (!$adj->guardar()) {
                return false;
            }
        }
    }
    return true;
}
